<?php

class test {
    public $abc=10;
    public $xyz=20;

    public function xyz() {
        echo 'hi';
    }
}

$objA = new test();
echo $objA->abc; //accessible from outside
echo $objA->xyz;
$objA->xyz(); //public method of the class test
