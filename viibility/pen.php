<?php
class pen {
    protected $pr;
    public $a;
    protected function testParent() {
        echo 'this is test';
    }
}

class child extends pen {
    public function testChild() {
        $this->testParent(); //will work because it
    }
}
$objParent = new pen();
//$objParent->testParent(); //Throw error
$objChild = new Child();
$objChild->testChild(); //work because test child will call test parent.
 