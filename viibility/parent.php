<?php

class parent {
    protected $pr;
    public $a;
    protected function testParent() {
        echo 'this is test';
    }
}

class child extends parent {
    public function testChild() {
        $this->testParent(); //will work because it
    }
}
$objParent = new parent();
$objParent->testParent(); //Throw error
$objChild = new Child();
$objChild->testChild(); //work because test child will call test parent.