<?php

Class test {
    public $abc;
    private $xyz;

    public function pubDo($a) {
        echo $a;
    }

    private function privDo($b) {
        echo $b;
    }

    public function pubPrivDo() {
        $this->xyz = 1;
        $this->privDo(1);
    }

}
$objT = new test();

$objT->abc = 3; //Works fine
//$objT->xyz = 1; //Throw fatal error of visibility
$objT->pubDo("test"); //Print "test"
//$objT->privDo(1); //Fatal error of visibility
$objT->pubPrivDo(); //Within this method private function privDo and variable xyz is called using $this variable.