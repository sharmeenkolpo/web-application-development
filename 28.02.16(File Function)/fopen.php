<?php
// "w" (Write only. Opens and clears the contents of file; or creates a new file if it doesn't exist)
$file = fopen("w.txt","w");
echo fwrite($file,"Write only. Opens and clears the contents of file; or creates a new file if it doesn't exist");
fclose($file);

//"w+" (Read/Write. Opens and clears the contents of file; or creates a new file if it doesn't exist)
$file = fopen("wplus.txt","w+");
echo fwrite($file,"Write only. Opens and clears the contents of file; or creates a new file if it doesn't exist");
fclose($file);

//"r" (Read only. Starts at the beginning of the file)
$file = fopen("r.txt","r");
$read=fread($file,filesize("r.txt"));
echo "$read";
fclose($file);

//"r+" (Read/Write. Starts at the beginning of the file)
$file = fopen("rplus.txt","r+");
$read=fread($file,filesize("rplus.txt"));
echo "$read";
fclose($file);

//"a" (Write only. Opens and writes to the end of the file or creates a new file if it doesn't exist)
$file = fopen("a.txt","a");
echo fwrite($file,"Write only. Opens and writes to the end of the file or creates a new file if it doesn't exist");
fclose($file);


//"a+" (Read/Write. Preserves file content by writing to the end of the file)
$file = fopen("aplus.txt","a+");
echo fwrite($file,"Write only. Opens and writes to the end of the file or creates a new file if it doesn't exist");
fclose($file);

//"x" (Write only. Creates a new file. Returns FALSE and an error if file already exists)
$file = fopen("x.txt","x");
echo fwrite($file,"Write only. Opens and clears the contents of file; or creates a new file if it doesn't exist");
fclose($file);


//"x+" (Read/Write. Creates a new file. Returns FALSE and an error if file already exists)
$file = fopen("xplus.txt","x+");
echo fwrite($file,"Write only. Opens and clears the contents of file; or creates a new file if it doesn't exist");
fclose($file);
