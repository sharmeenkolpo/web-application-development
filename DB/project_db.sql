-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2016 at 10:45 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors_tbl`
--

CREATE TABLE IF NOT EXISTS `actors_tbl` (
  `id` int(11) NOT NULL,
  `actor` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actors_tbl`
--

INSERT INTO `actors_tbl` (`id`, `actor`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'A', '0000-00-00', '0000-00-00', NULL),
(2, 'B', '0000-00-00', '0000-00-00', NULL),
(3, 'C,D', '0000-00-00', '0000-00-00', NULL),
(4, 'E,F', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `birth_tbl`
--

CREATE TABLE IF NOT EXISTS `birth_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_tbl`
--

INSERT INTO `birth_tbl` (`id`, `title`, `date`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'a', '0001-01-01', '0000-00-00', '0000-00-00', NULL),
(2, 'w', '0005-02-23', '0000-00-00', '0000-00-00', NULL),
(3, '3e24', '0000-00-00', '0000-00-00', '0000-00-00', NULL),
(4, '', '0000-00-00', '0000-00-00', '0000-00-00', NULL),
(5, 'werwer', '0000-00-00', '0000-00-00', '0000-00-00', NULL),
(6, 'rtr', '0000-00-00', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_tbl`
--

CREATE TABLE IF NOT EXISTS `book_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_tbl`
--

INSERT INTO `book_tbl` (`id`, `title`, `author_name`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'awredew', 'erfser', '0000-00-00', '0000-00-00', NULL),
(2, 'erfswer', 'erwesr', '0000-00-00', '0000-00-00', NULL),
(3, 'erfwesr', 'erwer', '0000-00-00', '0000-00-00', NULL),
(4, 'dfsdf', 'dsfsd', '0000-00-00', '0000-00-00', NULL),
(5, 'dfssd', 'sdfsdf', '0000-00-00', '0000-00-00', NULL),
(6, 'tttttttt', 'ttttttttt', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city_tbl`
--

CREATE TABLE IF NOT EXISTS `city_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_tbl`
--

INSERT INTO `city_tbl` (`id`, `title`, `created_at`, `modified _at`, `deleted_at`) VALUES
(12, 'Borishal', '0000-00-00', '0000-00-00', NULL),
(14, 'Dhaka', '0000-00-00', '0000-00-00', NULL),
(16, 'Khulna', '0000-00-00', '0000-00-00', NULL),
(17, 'Khulna', '0000-00-00', '0000-00-00', '2016-04-09'),
(18, 'sylhet', '0000-00-00', '0000-00-00', NULL),
(19, 'Rajshahi', '0000-00-00', '0000-00-00', NULL),
(20, 'Borishal', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender_tbl`
--

CREATE TABLE IF NOT EXISTS `gender_tbl` (
  `id` int(11) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender_tbl`
--

INSERT INTO `gender_tbl` (`id`, `gender`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'Female', '0000-00-00', '0000-00-00', NULL),
(2, 'Female', '0000-00-00', '0000-00-00', NULL),
(3, 'Female', '0000-00-00', '0000-00-00', NULL),
(4, 'Male', '0000-00-00', '0000-00-00', NULL),
(5, 'Male', '0000-00-00', '0000-00-00', NULL),
(6, 'Male', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `img_tbl`
--

CREATE TABLE IF NOT EXISTS `img_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `img_tbl`
--

INSERT INTO `img_tbl` (`id`, `title`, `img`, `created_at`, `modified _at`, `deleted_at`) VALUES
(2, 'asd', '1459797308m.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(3, 'ssss', '1459797331n.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(4, 'd', '1459765138a.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 'ttqq', '1459798069a.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(6, 'ww', '1459798083b.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(7, 'k', '1459794316images.jpg', '0000-00-00', '0000-00-00', '0000-00-00'),
(8, 'l', '1459794325index.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(9, 'aqw', '1459795060n.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(10, 'aaaaaaa', '1459799961a.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(11, 'ssdsdd', '1459800321n.png', '0000-00-00', '0000-00-00', '0000-00-00'),
(13, 'trtyu', '1459800387h.png', '0000-00-00', '0000-00-00', NULL),
(14, 'sefr', '1459800421images.jpg', '0000-00-00', '0000-00-00', NULL),
(15, 'shovon', '1459800432n.png', '0000-00-00', '0000-00-00', NULL),
(16, 'arif', '1459800444m.png', '0000-00-00', '0000-00-00', NULL),
(17, 'maria', '1459800454index.png', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mobiles_tbl`
--

CREATE TABLE IF NOT EXISTS `mobiles_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobiles_tbl`
--

INSERT INTO `mobiles_tbl` (`id`, `title`, `model`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'sdasd', 'asdasd', '0000-00-00', '0000-00-00', NULL),
(2, 'zsxdasd', 'asdasd', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscription_tbl`
--

CREATE TABLE IF NOT EXISTS `subscription_tbl` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_tbl`
--

INSERT INTO `subscription_tbl` (`id`, `email`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'a', '0000-00-00', '0000-00-00', '0000-00-00'),
(2, 'as@f.com', '0000-00-00', '0000-00-00', '0000-00-00'),
(3, 'ae@jh.j', '0000-00-00', '0000-00-00', '0000-00-00'),
(4, 'hg@jhiuh.com', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 'hg@jhiuh.com', '0000-00-00', '0000-00-00', NULL),
(6, 'as@f.com', '0000-00-00', '0000-00-00', '2016-04-05'),
(7, 'erwer@rgtf.hjkhj', '0000-00-00', '0000-00-00', NULL),
(8, 'ds@fdsdf.fcgdf', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summary_tbl`
--

CREATE TABLE IF NOT EXISTS `summary_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_tbl`
--

INSERT INTO `summary_tbl` (`id`, `title`, `summary`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'a', 'a', '0000-00-00', '0000-00-00', '0000-00-00'),
(2, 'w', 'w', '0000-00-00', '0000-00-00', '0000-00-00'),
(3, 'rr', 'r', '0000-00-00', '0000-00-00', '0000-00-00'),
(4, 'w', 'e', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 'tt', 'tt', '0000-00-00', '0000-00-00', '0000-00-00'),
(6, 'rtet', 'rtret', '0000-00-00', '0000-00-00', '2016-04-05'),
(7, 'trryrt', 'tyry', '0000-00-00', '0000-00-00', NULL),
(8, 'sdfds', 'dfsd', '0000-00-00', '0000-00-00', NULL),
(9, 'aaaaaaaaaaaaa', 'aaaaaaaa', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `terms_tbl`
--

CREATE TABLE IF NOT EXISTS `terms_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `terms` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified _at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms_tbl`
--

INSERT INTO `terms_tbl` (`id`, `title`, `terms`, `created_at`, `modified _at`, `deleted_at`) VALUES
(1, 'saaaaaaaaaaaaa', '1', '0000-00-00', '0000-00-00', NULL),
(2, 'zz', '1', '0000-00-00', '0000-00-00', NULL),
(3, 'cccccccccccccc', '1', '0000-00-00', '0000-00-00', NULL),
(4, 'fffffffffffff', '1', '0000-00-00', '0000-00-00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors_tbl`
--
ALTER TABLE `actors_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birth_tbl`
--
ALTER TABLE `birth_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_tbl`
--
ALTER TABLE `book_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_tbl`
--
ALTER TABLE `city_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender_tbl`
--
ALTER TABLE `gender_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `img_tbl`
--
ALTER TABLE `img_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles_tbl`
--
ALTER TABLE `mobiles_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_tbl`
--
ALTER TABLE `subscription_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_tbl`
--
ALTER TABLE `summary_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_tbl`
--
ALTER TABLE `terms_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actors_tbl`
--
ALTER TABLE `actors_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `birth_tbl`
--
ALTER TABLE `birth_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `book_tbl`
--
ALTER TABLE `book_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `city_tbl`
--
ALTER TABLE `city_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `gender_tbl`
--
ALTER TABLE `gender_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `img_tbl`
--
ALTER TABLE `img_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `mobiles_tbl`
--
ALTER TABLE `mobiles_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscription_tbl`
--
ALTER TABLE `subscription_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `summary_tbl`
--
ALTER TABLE `summary_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `terms_tbl`
--
ALTER TABLE `terms_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
