<?php
include_once '../../../../vendor/autoload.php';

use App\bitm\seip\pen\pen;
use App\bitm\seip\Utility\Utility;

$objpen=new pen();
$allpen=$objpen->trashed();//trashed data
$objutil=new Utility();
//$objutil->debug($allpen);
?>


<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Company</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($allpen) && !empty($allpen)) {
        $se = 0;
        foreach ($allpen as $onepen) {
            $se++;
            ?>

            <tr>
                <td><?php echo $se ?></td>
                <td><?php echo $onepen['id'] ?></td>
                <td><?php echo $onepen['title'] ?></td>
                <td><?php echo $onepen['company'] ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $onepen['id'] ?>">restore</a>|
                    <a href="delete.php?id=<?php echo $onepen['id'] ?>">delete</a>
                </td>
            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="5">No Data Found</td>
        </tr> 
<?php } ?>
</table>
