<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\bitm\seip\pen\pen;
use App\bitm\seip\Utility\Utility;
session_start();
$objpen = new pen();
$objpen->index();
$allpen = $objpen->index();
$util = new Utility();
//$util->debug($allpen);
if(isset($_SESSION['msg'])&&!empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}

?>
<a href="create.php">Create pen</a>
<a href="trashed.php">trashed data</a>
<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Company</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($allpen) && !empty($allpen)) {
        $se = 0;
        foreach ($allpen as $onepen) {
            $se++;
            ?>

            <tr>
                <td><?php echo $se ?></td>
                <td><?php echo $onepen['id'] ?></td>
                <td><?php echo $onepen['title'] ?></td>
                <td><?php echo $onepen['company'] ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $onepen['id'] ?>">edit</a>|
                    <a href="show.php?id=<?php echo $onepen['id'] ?>">show</a>|
<!--                    <a href="delete.php?id=<?php echo $onepen['id'] ?>">delete</a>-->
                    <a href="trash.php?id=<?php echo $onepen['id']?>&title=<?php echo $onepen['title']?>&company=<?php echo $onepen['company'] ?>">trash</a>
                </td>
            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="5">No Data Found</td>
        </tr> 
<?php } ?>
</table>
