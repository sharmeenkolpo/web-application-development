<?php

class MyClass{
    public  $prop1="I'm a class property";
    
    public function setProperty($newVal){
        $this->prop1=$newVal;
    }
    
    public function getProperty(){
        return  $this->prop1 . "<br/>";
    }
    
}
$obj= new MyClass;
$obj2=new MyClass;
//var_dump($obj);
//echo '<br>';
//echo $obj->prop1;
echo $obj->getProperty();
echo $obj2->getProperty();
echo '<br>';
$obj->setProperty("obj");
$obj2->setProperty("obj2");
echo $obj->getProperty();
echo $obj2->getProperty();
















