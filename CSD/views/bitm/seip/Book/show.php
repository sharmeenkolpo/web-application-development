<?php
use App\bitm\seip\Utility\Utility;
use App\bitm\seip\Book\Book;
include_once '../../../../vendor/autoload.php';

$getId=$_GET['id'];
//echo $getId;
$objBook=new Book();
$objBook->show($getId);
$id=$objBook->show($getId);

$objUtil=new Utility();
//$objUtil->debug($id);

?>

<a href="create.php">Add New</a> |
<a href="index.php">Back to List</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $id['id']?></td>
        <td><?php echo $id['title']?></td>
        <td><?php echo $id['created_at']?></td>
        <td><?php echo $id['modified_at']?></td>
        <td>
            <a href="edit.php">Edit</a> | 
            <a href="delete.php">Delete</a>
        </td>
    </tr>
</table>
