<?php
include_once '../../../../vendor/autoload.php';

use  App\Bitm\SEIP114596\Subscription\Subscription;
use  App\Bitm\SEIP114596\Utility\Utility;

$obj=new Subscription();
$obj->prepare($_POST);
$obj->store();
$util=new Utility();
$util->debug($_POST);
