<?php
include_once '../../../../vendor/autoload.php';

use  App\Bitm\SEIP114596\Birthday\Birthday;
use  App\Bitm\SEIP114596\Utility\Utility;

$objcls=new Birthday();
$objcls->prepare($_POST);
$objcls->store();
$util=new Utility();
$util->debug($_POST);
