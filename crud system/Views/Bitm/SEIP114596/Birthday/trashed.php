<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Birthday\Birthday;
use App\Bitm\SEIP114596\Utility\Utility;

$objcls=new Birthday();
$all=$objcls->trashed();//trashed data
$objutil=new Utility();
//$objutil->debug($allpen);
?>
<a href="create.php">Create Birthday</a>
<a href="index.php">Back to list page</a>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Date of Birth</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($all) && !empty($all)) {
        $se = 0;
        foreach ($all as $one) {
            $se++;
            ?>

            <tr>
                <td><?php echo $se ?></td>
                <td><?php echo $one['id'] ?></td>
                <td><?php echo $one['title'] ?></td>
                <td><?php echo $one['date'] ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $one['id'] ?>">restore</a>|
                    <a href="delete.php?id=<?php echo $one['id'] ?>">delete</a>
                </td>
            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="5">No Data Found</td>
        </tr> 
<?php } ?>
</table>
