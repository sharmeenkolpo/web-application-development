<?php
include_once '../../../../vendor/autoload.php';

use  App\Bitm\SEIP114596\Book\Book;
use  App\Bitm\SEIP114596\Utility\Utility;

$objBook=new Book();
$objBook->prepare($_POST);
$objBook->store();
$util=new Utility();
$util->debug($_POST);
