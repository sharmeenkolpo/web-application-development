<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Book\Book;
use App\Bitm\SEIP114596\Utility\Utility;

$getid = $_GET['id'];
$objBook = new Book();
$objBook->show($getid);
$showOne = $objBook->show($getid);
$objBook->edit();
$util = new Utility();
//$util->debug($showOne);
?>
<a href="create.php">Create Book</a>
<a href="index.php">Back to home</a><br/>
<form action="update.php" method="post">
    <fieldset>
        <legend>edit Pen List</legend>
        <input type="text" name="title" id="title" value="<?php echo $showOne['title'] ?>"/><br/>
        <input type="hidden" name="id" value="<?php echo $getid ?>"/>
        <input type="text" name="author_name" id="author_name" value="<?php echo $showOne['author_name'] ?>"/><br/>
        <input type="submit" value="Update"/>
    </fieldset>
</form>
