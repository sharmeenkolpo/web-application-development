<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\City\City;
use App\Bitm\SEIP114596\Utility\Utility;

$objBook=new City();
$allBook=$objBook->trashed();//trashed data
$objutil=new Utility();
//$objutil->debug($allpen);
?>
<a href="create.php">Create </a>
<a href="index.php">Back to list page</a>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($allBook) && !empty($allBook)) {
        $se = 0;
        foreach ($allBook as $oneBook) {
            $se++;
            ?>

            <tr>
                <td><?php echo $se ?></td>
                <td><?php echo $oneBook['id'] ?></td>
                <td><?php echo $oneBook['title'] ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $oneBook['id'] ?>">restore</a>|
                    <a href="delete.php?id=<?php echo $oneBook['id'] ?>">delete</a>
                </td>
            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="5">No Data Found</td>
        </tr> 
<?php } ?>
</table>
