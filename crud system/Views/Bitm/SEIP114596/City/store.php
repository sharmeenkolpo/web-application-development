<?php
include_once '../../../../vendor/autoload.php';

use  App\Bitm\SEIP114596\City\City;
use  App\Bitm\SEIP114596\Utility\Utility;

$obj=new City();

$obj->prepare($_POST);
$obj->store();
$util=new Utility();
$util->debug($_POST);
