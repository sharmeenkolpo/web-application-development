<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Terms\Terms;
use App\Bitm\SEIP114596\Utility\Utility;
session_start();
$objcls=new Terms();
$all=$objcls->trashed();//trashed data
$objutil=new Utility();
//$objutil->debug($allpen);
if(isset($_SESSION['msg'])&&!empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}


?>
<a href="create.php">Terms</a>
<a href="index.php">Back to list page</a>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
         <th>Title</th>
        <th>Terms</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($all) && !empty($all)) {
        $se = 0;
        foreach ($all as $one) {
            $se++;
            ?>

            <tr>
                <td><?php echo $se ?></td>
                <td><?php echo $one['id'] ?></td>
                <td><?php echo $one['title'] ?></td>
                 <td><?php echo $one['terms'] ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $one['id'] ?>">restore</a>|
                    <a href="delete.php?id=<?php echo $one['id'] ?>">delete</a>
                </td>
            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="5">No Data Found</td>
        </tr> 
<?php } ?>
</table>
