<?php
include_once '../../../../vendor/autoload.php';

use  App\Bitm\SEIP114596\Summary\Summary;
use  App\Bitm\SEIP114596\Utility\Utility;

$obj=new Summary();
$obj->prepare($_POST);
$obj->store();
$util=new Utility();
$util->debug($_POST);
