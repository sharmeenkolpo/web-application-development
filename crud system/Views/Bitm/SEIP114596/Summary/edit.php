<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Summary\Summary;
use App\Bitm\SEIP114596\Utility\Utility;

$getid = $_GET['id'];
$objBook = new Summary();
$objBook->show($getid);
$showOne = $objBook->show($getid);
$objBook->edit();
$util = new Utility();
//$util->debug($showOne);
?>
<a href="create.php">Create Summary</a>
<a href="index.php">Back to home</a><br/>
<form action="update.php" method="post">
    <fieldset>
        <legend>edit Summary</legend>
        <input type="text" name="title" id="title" value="<?php echo $showOne['title'] ?>"/><br/><br/>
        <input type="hidden" name="id" value="<?php echo $getid ?>"/>
        <input type="text" name="summary" id="summary" value="<?php echo $showOne['summary'] ?>"/><br/>
        <input type="submit" value="Update"/>
    </fieldset>
</form>
