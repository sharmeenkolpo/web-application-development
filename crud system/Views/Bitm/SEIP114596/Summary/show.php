<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Summary\Summary;
use App\Bitm\SEIP114596\Utility\Utility;
//$allpen=$objpen->index();

$getid=$_GET['id'];
$objBook=new Summary();
$objBook->show($getid);
$showOne=$objBook->show($getid);
$util=new Utility();
//$util->debug($showOne);

?>
<a href="create.php">Create Book</a>
<a href="index.php">Back to home</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Summary</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $showOne['id'] ?></td>
        <td><?php echo $showOne['title'] ?></td>
        <td><?php echo $showOne['summary'] ?></td>
        <td>
            <a href="edit.php">edit</a>|
            <a href="delete.php">delete</a>
        </td>
    </tr>
</table>
