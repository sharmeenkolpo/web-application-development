<?php
include_once '../../../../vendor/autoload.php';

use  App\Bitm\SEIP114596\Profile\Profile;
use  App\Bitm\SEIP114596\Utility\Utility;

$objcls=new Profile();
$util=new Utility();
//$util->debug($_POST);
//$util->debug($_FILES);

$imgExt=array("png","jpg","gif","jpeg");


if(isset($_FILES['img'])){
    $errors=array();
    $img_name=  time().$_FILES['img']['name'];
    $img_type=$_FILES['img']['type'];
    $img_tmp_name=$_FILES['img']['tmp_name'];
    $img_size=$_FILES['img']['size'];
    $imginfo=  explode('.', $img_name);
//    $util->debug($imginfo);
    
    $myExt=  strtolower(end($imginfo));
//    $util->debug($myExt);
    if(in_array($myExt, $imgExt)==FALSE){
        $errors['er']="Invalid format";
        //$util->debug($errors['er']);
    }  else {
//        echo 'valid';
        $_POST['img_name']=$img_name;
        move_uploaded_file( $img_tmp_name, "../../../../image/".$img_name);
    }
    
}
$objcls->prepare($_POST);
$objcls->store();
//$util->debug($_POST);








