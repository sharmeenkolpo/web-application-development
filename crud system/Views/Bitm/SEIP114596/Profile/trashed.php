<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Profile\Profile;
use App\Bitm\SEIP114596\Utility\Utility;

session_start();
$objcls = new Profile();
$objcls->trashed();
$all = $objcls->trashed();
$util = new Utility();
//$util->debug($all);
//$util->debug($all);
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
<div class="body_class">
<div class="ancor">
<a  href="create.php">Create Profile</a>
<a  href="trashed.php">trashed data</a>
</div>
<center>
    <table border="1" class="table_class">
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Title</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        <?php
        if (isset($all) && !empty($all)) {
            $se = 0;
            foreach ($all as $one) {
                $se++;
                ?>

                <tr>
                    <td><?php echo $se ?></td>
                    <td><?php echo $one['id'] ?></td>
                    <td><?php echo $one['title'] ?></td>
                    <td><img src="<?php echo "../../../../image/".$one['img'] ?>"</img></td>
                    <td>
                        <a href="edit.php?id=<?php echo $one['id'] ?>">edit</a>|
                        <a href="show.php?id=<?php echo $one['id'] ?>">show</a>|
        <!--                    <a href="delete.php?id=<?php echo $one['id'] ?>">delete</a>-->
                        <a href="trash.php?id=<?php echo $one['id'] ?>">delete</a>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="5">No Data Found</td>
            </tr> 
<?php } ?>
    </table>
</center>
    </div>