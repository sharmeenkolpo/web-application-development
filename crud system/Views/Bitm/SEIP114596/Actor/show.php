<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Actor\Actor;
use App\Bitm\SEIP114596\Utility\Utility;

//$allpen=$objpen->index();

$getid = $_GET['id'];
$objcls = new Actor();
$objcls->show($getid);
$showOne = $objcls->show($getid);
$util = new Utility();
//$util->debug($showOne);
?>
<a href="create.php">Actor List</a>
<a href="index.php">Back to home</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $showOne['id'] ?></td>
        <td><?php echo $showOne['actor'] ?></td>
        <td>
            <a href="edit.php">edit</a>|
            <a href="delete.php">delete</a>
        </td>
    </tr>
</table>
