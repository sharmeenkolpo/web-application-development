
<?php
//
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP114596\Mobile\Mobile;
use App\Bitm\SEIP114596\Utility\Utility;

session_start();
$objcls = new Mobile();
$objcls->index();
$all = $objcls->index();
$util = new Utility();
//$util->debug($allpen);
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
<html>
    <head>
        <title>Mini Project</title>
        <link type="text/css" rel="stylesheet" href="../../../../style.css" />
    </head>
    <body>
        <div class="main_div">
             <div class="header_div">
                <h1>Mini Project List </h1><br/>
                <h2>Select Anyone From The Menu</h2><br/>
            </div>
            <div class="wrapper_div">
                <div class="menu_div">
                    <ul>
                         <a href="../../../../index.php"><li>Home</li></a>
                        <a href="../Book/index.php"><li>Book Title</li></a>
                        <a href="../Birthday/index.php"><li>Birthday</li></a> 
                        <a href="../Summary/index.php"><li>Summary of Organization</li></a> 
                        <a href="../Subscription/index.php"><li>Email Subscription</li></a>
                        <a href="../Profile/index.php"><li>Profile</li></a>
                        <a href="../Gender/index.php"><li>Gender</li></a>
                        <a href="../Terms/index.php"><li>Terms</li></a>                    
                        <a href="../Actor/index.php"><li>Actor</li></a>
                        <a href="../Mobile/index.php"><li>Mobile</li></a>
                        <a href="../City/index.php"><li>City</li></a>
                    </ul>
                </div>
            </div>


            <div class="body_class">
                <div class="ancor">
                    <a  href="create.php">Create </a>
                    <a  href="trashed.php">Trashed data</a>
                </div>
                <table class="table_class" border="1">
                    <tr>
                        <th>SL</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Model</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    if (isset($all) && !empty($all)) {
                        $se = 0;
                        foreach ($all as $one) {
                            $se++;
                            ?>

                            <tr>
                                <td><?php echo $se ?></td>
                                <td><?php echo $one['id'] ?></td>
                                <td><?php echo $one['title'] ?></td>
                                <td><?php echo $one['model'] ?></td>
                                <td>
                                    <a href="edit.php?id=<?php echo $one['id'] ?>">edit</a>|
                                    <a href="show.php?id=<?php echo $one['id'] ?>">show</a>|
                                    <!--<a href="delete.php?id=<?php echo $one['id'] ?>">delete</a>-->

                                    <a href="trash.php?id=<?php echo $one['id'] ?>">delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="5">No Data Found</td>
                        </tr> 
                    <?php } ?>
                </table>
            </div>
        </div>
    </body>
</html>