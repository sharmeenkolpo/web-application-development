<?php
namespace App\Bitm\SEIP114596\Summary;

class Summary {
    
    public $id = '';
    public $title = '';
    public $summary = '';
//INSERT INTO `project_db`.`summary_tbl` (`id`, `title`, `summary`, `created_at`, `modified _at`, `deleted_at`) VALUES (NULL, 'a', 'a', '', '', '');
    public function __construct() {
        $conn = mysql_connect("localhost", "root", "")or die("not connect");
        mysql_select_db("project_db")or die("not connect DB");
    }

    public function prepare($data = '') {
         if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        } else {
            echo 'id mising';
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        } else {
            echo 'no title';
        }
       
        if (isset($data['summary']) && !empty($data['summary'])) {
            $this->summary = $data['summary'];
        } else {
            echo 'summary';
        }
    }

    public function store() {
         session_start();
        if ( isset($this->title) && !empty($this->title)&&isset($this->summary) && !empty($this->summary)) {
           $query = "INSERT INTO `project_db`.`summary_tbl` (`id`, `title`, `summary`) VALUES (NULL, '".$this->title."', '".$this->summary."')";
           
            if (mysql_query($query)) {
                $_SESSION['msg'] = 'data successfully inserted';
                header("location:create.php");
            } else {
                $_SESSION['msg'] = 'data not inserted';
                header("location:create.php");
            }
        } else {
            $_SESSION['msg'] = "Not submited:Data required";
            header('location:create.php');
        }
        header('location:index.php');
    }

    public function index() {
        $all = array();
        $query = "SELECT * FROM `summary_tbl` WHERE deleted_at is NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        return $all;
    }


    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `summary_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function delete($id = '') {
        $this->id = $id;
        $query = "DELETE FROM `summary_tbl` WHERE id=$this->id ";
        mysql_query($query);
        header("location:trashed.php");
    }

    public function edit($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `summary_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function update() {
        $query = "UPDATE `project_db`.`summary_tbl` SET `title` = '" . $this->title . "', `summary` = '".$this->summary."'  WHERE `summary_tbl`.`id` = $this->id";
        if (mysql_query($query)) {
            echo 'Updated';
            header("location:index.php");
        } else {
            echo 'Error';
        }
    }
    public function trash($id='') {
        $this->id=$id;
        $query = "UPDATE `project_db`.`summary_tbl` SET `deleted_at` = '" . date('Y-m-d') . "' WHERE `summary_tbl`.`id` = $this->id";
        //echo $query;
        mysql_query($query);
        header("location:index.php");
    }
    public function trashed() {
        $all = array();
        $query = "SELECT * FROM `summary_tbl` WHERE deleted_at is NOT NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        //header("location:trashed.php");
        return $all;
        
    }
}
