<?php

namespace App\Bitm\SEIP114596\Profile;

class Profile {

    public $id = '';
    public $title = '';
    public $img = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '')or die('no connection');
        mysql_select_db("project_db")or die('no connection with DB');
    }

    public function prepare($data = '') {
        if (array_key_exists('id',$data)&&!empty($data['id'])) {
            $this->id = $data['id'];
        } else {
            echo "no id";
        }
        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $this->title = $data['name'];
        } else {
            echo "no title";
        }
        if (array_key_exists('img_name', $data) && !empty($data['img_name'])) {
            $this->img = $data['img_name'];
        } else {
            echo "no img";
        }
    }

    public function store() {
        $query = "INSERT INTO `project_db`.`img_tbl` (`id`, `title`, `img`) VALUES (NULL, '" . $this->title . "', '" . $this->img . "')";
        if (mysql_query($query)) {
            echo 'data inserted';
        } else {
            echo 'data not inserted';
        }
    }

    public function index() {
        $all = array();
        $query = "SELECT * FROM `img_tbl` WHERE deleted_at is NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        return $all;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `img_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function delete($id = '') {
        session_start();
        $this->id = $id;
        $query = "DELETE FROM `img_tbl` WHERE id=$this->id ";
        mysql_query($query);
        //header("location:index.php");
        $_SESSION['msg'] = "deleted successfully from DB";
        header("location:trashed.php");
    }

    public function edit($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `img_tbl` WHERE id=$this->id ";
        echo $query;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function update() {

        session_start();//UPDATE `project_db`.`img_tbl` SET `title` = 'tt', `img` = '12a.png' WHERE `img_tbl`.`id` = 5;
        if(isset($this->img)&&!empty($this->img)){
            $query = "UPDATE `project_db`.`img_tbl` SET `title` = '".$this->title."', `img` = '".$this->img."' WHERE `img_tbl`.`id` = $this->id";
        }  else {
            $query = "UPDATE `project_db`.`img_tbl` SET `title` = '".$this->title."' WHERE `img_tbl`.`id` = $this->id";
        }
        echo $query;
        if (mysql_query($query)) {
            $_SESSION['msg'] = 'Updated successfully';
            header("location:index.php");
        } else {
            //$_SESSION['msg'] = 'Error in Update data';
            // header("location:index.php");
        }
    }

    public function trash($id = '') {
        session_start();
        $this->id = $id;
        $query = "UPDATE `project_db`.`img_tbl` SET `deleted_at` = '" . date('Y-m-d') . "' WHERE `img_tbl`.`id` = $this->id";
        echo $query;
       // die();
        mysql_query($query);
        $_SESSION['msg'] = "deleted successfully";
        header("location:index.php");
    }

    public function trashed() {
        //session_start();
        $all = array();
        $query = "SELECT * FROM `img_tbl` WHERE deleted_at is NOT NULL";
        echo $query;
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        //$_SESSION['msg'] = 'Deleted form DB';
//        header("location:index.php");
        return $all;
    }

}
