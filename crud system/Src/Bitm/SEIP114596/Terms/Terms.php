<?php

namespace App\Bitm\SEIP114596\Terms;

class Terms {

    public $id = '';
    public $title = '';
    public $terms =0;

    public function __construct() {
        $conn = mysql_connect("localhost", "root", "") or die("not connect");
        mysql_select_db("project_db") or die("not connect DB");
    }

    public function prepare($data = '') {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        } else {
            echo 'id mising';
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        } else {
            echo 'title mising';
        }
        if (isset($data['terms']) && !empty($data['terms'])) {
            $this->terms = 1;
        } else {
              $this->terms = $data['terms'];
        }
    }

    public function store() {
        session_start();
        if (isset($this->title) && !empty($this->title) && isset($this->terms) && !empty($this->terms)) {
            $query = "INSERT INTO `project_db`.`terms_tbl` (`id`, `title`,`terms`) VALUES (NULL, '" . $this->title . "','" . $this->terms . "')";
            echo $query;
            if (mysql_query($query)) {
                $_SESSION['msg'] = 'data successfully inserted';
                header("location:create.php");
            } else {
                $_SESSION['msg'] = 'data not inserted';
                header("location:create.php");
            }
        } else {
            $_SESSION['msg'] = "Not submited";
            header('location:create.php');
        }
        header('location:index.php');
    }

    public function index() {
        $all = array();
        $query = "SELECT * FROM `terms_tbl`  WHERE deleted_at is NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        return $all;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `terms_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function delete($id = '') {
        session_start();
        $this->id = $id;
        $query = "DELETE FROM `terms_tbl` WHERE id=$this->id ";
        mysql_query($query);
        //header("location:index.php");
        $_SESSION['msg'] = "deleted successfully from DB";
        header("location:trashed.php");
    }

    public function edit($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `terms_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function update() {
        session_start();//UPDATE `terms`.`terms_tbl` SET `title` = 'ppppppa', `terms` = '0' WHERE `terms_tbl`.`id` = 20;
        $query = "UPDATE `project_db`.`terms_tbl` SET `title` = '" . $this->title . "',`terms` = '" . $this->terms . "'  WHERE `terms_tbl`.`id` = $this->id";
        if (mysql_query($query)) {
            $_SESSION['msg'] = 'Updated successfully';
            header("location:index.php");
        } else {
            //$_SESSION['msg'] = 'Error in Update data';
           // header("location:index.php");
        }
    }

    public function trash($id = '') {
        session_start();
        $this->id = $id;
        $query = "UPDATE `project_db`.`terms_tbl` SET `deleted_at` = '" . date('Y-m-d') . "' WHERE `terms_tbl`.`id` = $this->id";
        //echo $query;
        mysql_query($query);
        $_SESSION['msg'] = "deleted successfully";
        header("location:index.php");
    }

    public function trashed() {
        //session_start();
        $all = array();
        $query = "SELECT * FROM `terms_tbl` WHERE deleted_at is NOT NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        //$_SESSION['msg'] = 'Deleted form DB';
//        header("location:index.php");
        return $all;
    }
}
