<?php

namespace App\Bitm\SEIP114596\Mobile;

class Mobile {

    public $id = '';
    public $title = '';
    public $model = '';

    public function __construct() {
        $conn = mysql_connect("localhost", "root", "")or die("not connect");
        mysql_select_db("project_db")or die("not connect DB");
    }

    public function prepare($data = '') {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        } else {
            echo 'id mising';
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        } else {
            echo 'title mising';
        }

        if (isset($data['model']) && !empty($data['model'])) {
            $this->model = $data['model'];
        } else {
            echo 'model mising';
        }
    }

    public function store() {
        session_start();
        if (isset($this->title) && !empty($this->title) && isset($this->model) && !empty($this->model)) {
            $query = "INSERT INTO `project_db`.`mobiles_tbl` (`id`, `title`, `model`) VALUES (NULL, '" . $this->title . "', '" . $this->model . "')";
            //echo $query;
            if (mysql_query($query)) {
                $_SESSION['msg'] = 'data successfully inserted';
                header("location:create.php");
            } else {
                $_SESSION['msg'] = 'data not inserted';
                header("location:create.php");
            }
        } else {
            $_SESSION['msg'] = "Not submited";
            header('location:create.php');
        }
        header('location:index.php');
    }

    public function index() {
        $all = array();
        $query = "SELECT * FROM `mobiles_tbl` WHERE deleted_at is NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        return $all;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `mobiles_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function delete($id = '') {
        session_start();
        $this->id = $id;
        $query = "DELETE FROM `mobiles_tbl` WHERE id=$this->id ";
        mysql_query($query);
        //header("location:index.php");
        $_SESSION['msg'] = "deleted successfully from DB";
        header("location:trashed.php");
    }

    public function edit($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `mobiles_tbl` WHERE id=$this->id ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function update() {
        session_start();
        $query = "UPDATE `project_db`.`mobiles_tbl` SET `title` = '" . $this->title . "', `model` = '" . $this->model . "'  WHERE `mobiles_tbl`.`id` = $this->id";
        if (mysql_query($query)) {
            $_SESSION['msg'] = 'Updated successfully';
            header("location:index.php");
        } else {
            $_SESSION['msg'] = 'Error in Update data';
            header("location:index.php");
        }
    }

    public function trash($id = '') {
        session_start();
        $this->id = $id;
        $query = "UPDATE `project_db`.`mobiles_tbl` SET `deleted_at` = '" . date('Y-m-d') . "' WHERE `mobiles_tbl`.`id` = $this->id";
        //echo $query;
        mysql_query($query);
        $_SESSION['msg'] = "deleted successfully";
        header("location:index.php");
    }

    public function trashed() {
        //session_start();
        $all = array();
        $query = "SELECT * FROM `mobiles_tbl` WHERE deleted_at is NOT NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $all[] = $row;
        }
        //$_SESSION['msg'] = 'Deleted form DB';
//        header("location:index.php");
        return $all;
    }
    public function restore() {
        session_start();
        $this->id = $id;
        $query = "UPDATE `project_db`.`mobiles_tbl` SET `deleted_at` = '" . date('Y-m-d') . "' WHERE `mobiles_tbl`.`id` = $this->id";
        //echo $query;
        mysql_query($query);
        $_SESSION['msg'] = "restore successfully";
        header("location:index.php");
    }
}
