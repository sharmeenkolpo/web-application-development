<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$text   = "\t\tThese are a few words :) ...  ";
$binary = "\x09Example string\x0A";
$hello  = "              Hello   World                           ";
var_dump($text, $binary, $hello);

print "\n";
echo "<br>";
$a=trim($hello);
var_dump(trim($hello));
echo "<br>";
var_dump(ltrim($a,"He"));
echo "<br>";
var_dump(rtrim($a,"ld"));
echo "<br>";

var_dump(trim($hello));


echo "<br>";

$trimmed = trim($text);
var_dump($trimmed);

echo "<br>";

$trimmed = trim($text, " \t.");
var_dump($trimmed);

echo "<br>";

$trimmed = trim($hello, "Hdle");
var_dump($trimmed);

echo "<br>";

$trimmed = trim($hello, 'HdWr');
var_dump($trimmed);

echo "<br>";

// trim the ASCII control characters at the beginning and end of $binary
// (from 0 to 31 inclusive)
$clean = trim($binary, "\x00..\x1F");
var_dump($clean);