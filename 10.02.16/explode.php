<?php


$input1 = "hello";
$input2 = "hello,there";
echo '<pre>';
var_dump( explode( ',', $input1 ) );
echo '</pre>';
echo '<pre>';
var_dump( explode( ',', $input2 ) );
echo '</pre>';



// Example 2
$data = "foo:*:1023:1000::/home/foo:/bin/sh";
list($user, $pass, $uid, $gid, $gecos, $home, $shell) = explode(":", $data);
echo $user; // foo
echo $pass; // *

echo '<pre>';
var_dump( explode( ':', $data ) );
echo '</pre>';