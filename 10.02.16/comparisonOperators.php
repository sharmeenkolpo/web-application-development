<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$a = 3;
$b = 4;

echo -$a . "<br>";
echo $a + $b . "<br>";
echo $a - $b . "<br>";
echo $a * $b . "<br>";
echo $a / $b . "<br>";
echo $a % $b . "<br>";

if ($a == $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a === $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a !== $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a <> $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a > $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a < $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a >= $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

if ($a <= $b) {
    echo 'yes';
} else {
    echo 'No';
}
echo "<br>";

