<?php

$array = array(0 => 0, "color" => "red");
echo '<pre>';
print_r(array_keys($array));
echo '</pre>';
$array = array("blue", "red", "green", "blue", "blue");
echo '<pre>';
print_r(array_keys($array, "blue"));
echo '</pre>';

$array = array("color" => array("blue", "red", "green"),
               "size"  => array("small", "medium", "large"));
echo '<pre>';
print_r(array_keys($array));
echo '</pre>';