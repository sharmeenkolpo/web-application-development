<?php

$input  = array("a", 4.0, array("c", "k"));
$reversed = array_reverse($input);
$preserved = array_reverse($input, true);
echo "<pre>";
print_r($input);
echo "</pre>";
echo "<pre>";
print_r($reversed);
echo "</pre>";
echo "<pre>";
print_r($preserved);
echo "</pre>";