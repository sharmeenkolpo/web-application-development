<?php

function odd($var){
    return ($var & 1);//1 & 1= 1, 0 & 1= 0
}
function even($var){
    return (!($var & 1));
}
$a=array('a'=>1,'b'=>2,'c'=>3);
$b=array(4,5,6);
echo "odd:\n";
echo "<pre>";
print_r(array_filter($a,"odd"));
//print_r(array_filter($b, $callback));
echo "<pre>";
echo "even:\n";
echo "<pre>";
print_r(array_filter($b,"even"));
echo "<pre>";
